package softserve.academy.AdminTests;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import softserve.academy.actions.AdminPanelActions;
import softserve.academy.models.Group;
import softserve.academy.models.GroupDataProvider;
import softserve.academy.models.UserDataProvider;

@Test(groups = {"admin", "funcCreateDelete", "group"})
public class AdminCreateDeleteGroupTest extends AdminBaseTest{
    @Test(dataProvider = "GroupDataProvider")
    public void createDeleteGroups(Group group){
        new AdminPanelActions()
                .openAdminPanel()
                .openGroupTab()
                .openCreateModal()
                .fillAllGroupFields(group)
                .submitCreateEdit()
                .verifySoftRowWithNamePresentInTab(group.getName(), "groups")
                .verifyCellWithTextPresentInRow(group.getDirection())
                .verifyCellWithTextPresentInRow(group.getLocation())
                .verifyCellWithTextPresentInRow(group.getTeacher())
                .verifyCellWithTextPresentInRow(group.getExpert())
                .verifyCellWithTextPresentInRow(group.getStartDate())
                .verifyCellWithTextPresentInRow(group.getFinishDate())
                .deleteRowByName(group.getName())
                .verifyRowWithNameNotPresentInTab(group.getName(), "groups")
                .assertAll();

    }


    @DataProvider(name="GroupDataProvider")
    public Object[][] groupCreateData() {
        return new Object[][]{
                {GroupDataProvider.DP_Current1},
                {GroupDataProvider.DP_Current2}
        };
    }

}
