package softserve.academy.Story_3_02;

import org.testng.annotations.*;
import softserve.academy.actions.AdminPanelActions;
import softserve.academy.actions.LoginPageActions;
import softserve.academy.actions.StudentsPageActions;
import softserve.academy.modal_windows.MWEditStudentlistActions;
import softserve.academy.models.GroupDataProvider;
import softserve.academy.models.Student;
import softserve.academy.models.User;

import static softserve.academy.models.Utils.getDriver;
import static softserve.academy.models.Utils.setDriver;

@Test(groups = {"createStudentsListInNewGroup"})
public class us302createStudentRecordInNewGroup {

    @BeforeTest(description = "Preconditions for create Students List")
    public void setUpForOrdinaryTest() {

        setDriver()
                .manage()
                .window()
                .maximize();
        new LoginPageActions()
                .openLoginPage()
                .waitLogInPageLoad()
                .login(new User("dmytro", "1234"))
                .waitPageLoaded();
        new AdminPanelActions()
                .openAdminPanel()
                .openGroupTab()
                .openCreateModal()
                .fillAllGroupFields(GroupDataProvider.forFillingStudentsList)
                .submitCreateEdit()
                .escapeToGroupPage()
                .openStudentsPage()
                .selectFutureGroups()
                .selectGroup(GroupDataProvider.forFillingStudentsList.getName());
    }

    @BeforeMethod
    public void setStudPage(){
        new StudentsPageActions()
                .editStudentList();
    }

    @DataProvider(name = "provideStudentData")
    public Object[][] groupStudents() {
        return new Object[][]{
                {new Student("Irina", "Svitlenko", 0, 2)},
                {new Student("Maksim", "Nikitghenko", 1, 3)},
        };
    }


    @Test(dataProvider = "provideStudentData", description = "User story 3-02:  Coordinator is on the “Group” page -> " +
            "Main menu -> Navigate to “Students” pageUser -> select group in group list -> Cogwheel -> " +
            "‘Edit Student list” modal window -> “Edit Student list”  ->  “Edit Student list” modal window -> " +
            "“+” for editing should open “Create/Edit student” modal window -> creating sdudents")
    public void createNewStudents(Student student) {
        new MWEditStudentlistActions()
                .clickCreateStudent()
                .setAllStudentRequiredFields(student)
                .verifyPresenceStudent(student)
                .assertAll();
    }

    @AfterMethod
    public void closeMWEditStudentList() {
        new MWEditStudentlistActions()
                .clickIconExit();

    }

    @AfterTest(alwaysRun = true)
    public void tearDown() {
        new StudentsPageActions()
                .logOut();
        getDriver().quit();
    }
}
