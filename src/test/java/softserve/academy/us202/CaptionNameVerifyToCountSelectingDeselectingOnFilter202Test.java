package softserve.academy.us202;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import softserve.academy.actions.SchedulePageActions;
import softserve.academy.models.Filter;
import softserve.academy.us101.BaseTest;

public class CaptionNameVerifyToCountSelectingDeselectingOnFilter202Test extends BaseTest {



	@Test(groups = {"schedulePage2"}, description = "Caption Name Verify To Count Selecting Deselecting On Filter pages" +
			" with verify ability to save selected state tabs on filter pages")
	public void testCounterOnFilterPages() {

		new SchedulePageActions()
				.clickFilter(Filter.FINISHED)
				.selectGroup("DP-092-NET")
				.clickFilter(Filter.CURRENT)
				.selectGroup("DP-094-MQC")
				.clickFilter(Filter.PLANNED)
				.selectGroup("DP-095-JS")
				.verifyСounterOfSelectedGroups(3)
				.verifySelectedGroup("DP-095-JS")
				.deselectGroup("DP-095-JS")
				.verifyСounterOfSelectedGroups(2)
				.clickFilter(Filter.CURRENT)
				.verifySelectedGroup("DP-094-MQC")
				.deselectGroup("DP-094-MQC")
				.verifyСounterOfSelectedGroups(1)
				.clickFilter(Filter.FINISHED)
				.verifySelectedGroup("DP-092-NET")
				.deselectGroup("DP-092-NET")
				.verifyСounterOfSelectedGroups(0)
				.assertAll();

	}
}
